import { Component, OnInit } from '@angular/core';
import { CategoriaService } from '../../../../http-service/categoria.service';
import { Categoria } from '../../../../shared/model/categoria.model';

@Component({
  selector: 'app-home-categorias',
  templateUrl: './home-categorias.component.html',
  styleUrls: ['./home-categorias.component.scss']
})
export class HomeCategoriasComponent implements OnInit {
  public categorias: Categoria[] = [];

  constructor(private readonly categoriaService: CategoriaService,
    ) { }

  ngOnInit(): void {
    this.cargarCategorias();
  }

  private cargarCategorias() {
    this.categorias = [];
    this.categoriaService.obtenerCategoria().subscribe({
      next: (response) => {
        console.log(response);
        this.categorias = response;
      }
    })
  }

}
