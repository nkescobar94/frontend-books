import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './core/layout/main-layout/main-layout.component';
import { CategoriasComponent } from './feature/categorias/categorias.component';

const routes: Routes = [
 // { path: '', redirectTo: '/login', pathMatch: 'full' },


  {
    path: '', component: MainLayoutComponent,
  },
{
    path: 'home',
    component: MainLayoutComponent,

    children: [
      {
        path: 'gestionar-categoria',
        loadChildren: () => import('./feature/categorias/categorias.module').then((m) => m.CategoriasModule),
      },
    ]
},
 // { path: '**', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
