import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriasComponent } from './categorias.component';
import { HomeCategoriasComponent } from './pages/home-categorias/home-categorias.component';

const routes: Routes = [
  {
    path: '',
    component: CategoriasComponent,
    children: [
      {
        path: 'consulta',
        component: HomeCategoriasComponent,
      },
      {
        path: '',
        redirectTo: 'consulta',
        pathMatch: 'full',
    },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriasRoutingModule { }
