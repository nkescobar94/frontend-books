import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriasRoutingModule } from './categorias-routing.module';
import { CategoriasComponent } from './categorias.component';
import { HomeCategoriasComponent } from './pages/home-categorias/home-categorias.component';


@NgModule({
  declarations: [
    CategoriasComponent,
    HomeCategoriasComponent
  ],
  imports: [
    CommonModule,
    CategoriasRoutingModule
  ]
})
export class CategoriasModule { }
