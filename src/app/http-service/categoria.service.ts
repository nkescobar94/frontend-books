import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map, Observable } from 'rxjs';
import { Categoria } from '../shared/model/categoria.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  private baseUrl: string;

  constructor(private httpClient: HttpClient) {
    this.baseUrl = `${environment.apiUrl}categorias`;
  }

  public obtenerCategoria(): Observable<Categoria[]> {
    return this.httpClient.get<Categoria[]>(`${this.baseUrl}`)
      .pipe(
        map<any, any>(categoria => categoria.categoriaResponse),
      )
      .pipe(
        map<any, Categoria[]>(categoria => categoria?.categoria),
      )
  }

}
